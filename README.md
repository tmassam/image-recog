# Image Recognition Service

The Image Recognition Service is a web application that utilizes AWS services and OpenAI API to perform image recognition and labeling. The application is built using React, and it integrates with Amazon Rekognition, AWS Lambda, Amazon DynamoDB, Amazon API Gateway, Amazon Cognito, Amazon Amplify, and Amazon S3. It is monitored and logged using Amazon Cloud Watch.

## Architecture Overview

1. Users authenticate using AWS Cognito and upload images through the ImageUploader component.
2. Images are stored in an Amazon S3 bucket.
3. The image-recog-labels Lambda function is triggered and processes the image using Amazon Rekognition and OpenAI API.
4. Labels and descriptions are stored in an Amazon DynamoDB table.
5. Users can query their previous image data using the get-previous-story Lambda function, which retrieves records from DynamoDB.

## Lambda Functions

### image-recog-labels.py

This is the code for the AWS Lambda function that uses [Amazon Rekognition](https://aws.amazon.com/rekognition/) and [OpenAI API](https://beta.openai.com/docs/) to perform image recognition and labeling. The function processes the input image and returns a set of labels describing the content. OpenAI then generates text using the input labels and a prompt.

[Link to image-recog-labels.py](https://gitlab.com/tmassam/image-recog/-/blob/main/src/image-recog-labels.py)

### get-previous-story.py

This Lambda function deals with querying the Amazon DynamoDB to retrieve historical image data for the user. It fetches relevant records from the database and returns them to the frontend.

[Link to get-previous-story.py](https://gitlab.com/tmassam/image-recog/-/blob/main/src/get-previous-story.py)

## React Components

* **ImageUploader**: Handles image uploading and processing.
* **Cognito.js**: Implements AWS Cognito for user authentication and management.
* **APIGateway.js**: Integrates with Amazon API Gateway to facilitate communication between the frontend and backend.
* **SkinPage.js**: A hardcoded page that uses AWS Amplify to interact with Amazon Rekognition locally for image recognition.
* **SkinPageAPI.js**: Demonstrates how to use Amazon API Gateway for image recognition requests.

AWS configuration is handled in the **index.js** file.

All React components can be found in the [src folder](https://gitlab.com/tmassam/image-recog/-/tree/main/src) of the public GitLab repository.

