import json
import os
import boto3
import openai
from urllib.parse import urlparse, parse_qs

print('Loading function')

rekognition = boto3.client('rekognition')
s3 = boto3.client('s3')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('UserStories')

def detect_labels(bucket, key):
    # Read the image bytes from the S3 bucket
    s3_response = s3.get_object(Bucket=bucket, Key=key)
    image_bytes = s3_response['Body'].read()

    response = rekognition.detect_labels(
        Image={"Bytes": image_bytes}
    )
    labels = [
        {"Confidence": str(label_prediction["Confidence"]), "Name": label_prediction["Name"]}
        for label_prediction in response["Labels"]
    ]
    return labels

def generate_story(labels):
    model_to_use = "text-davinci-003"
    input_prompt = "Once upon a time, there was a " + labels[0]['Name'] + " who lived in a " + labels[1]['Name'] + ". Describe a typical day in their life."
    openai.api_key = os.environ["OPENAI_API_KEY"]
    response = openai.Completion.create(
        model=model_to_use,
        prompt=input_prompt,
        temperature=0.7,
        max_tokens=400,
        top_p=1,
        frequency_penalty=0.0,
        presence_penalty=0.0
    )
    text_response = response['choices'][0]['text'].strip()
    return text_response

def lambda_handler(event, context):
    print(event)
    # Get the S3 bucket and object key from the event
    request_body = json.loads(event["body"])
    s3_bucket = "image-recog-labels"
    s3_key = request_body["key"]

    try:
        # Calls rekognition DetectLabels API to detect labels in the image from S3
        labels = detect_labels(s3_bucket, s3_key)
        print(labels)

        # Generate story based on labels
        story = generate_story(labels)
        print(story)

        return {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": True,
            },
            "body": json.dumps({"message": "Story generated successfully!", "story": story}),
        }
    except Exception as e:
        print(e)
        print("Error processing the image from S3.")
        raise e
