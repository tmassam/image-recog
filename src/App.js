import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import ConfirmSignUp from './pages/ConfirmSignUp';
import Dashboard from './pages/Dashboard';
import StoryPage from './pages/StoryPage'; 
import SkinPage from './pages/SkinPage';
import PreviousStories from './pages/PreviousStories';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/signin" element={<SignIn />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/confirm" element={<ConfirmSignUp />} />
        <Route path="/story" element={<StoryPage />} />
        <Route path="/skin" element={<SkinPage />} />
        <Route path="/previous-stories" element={<PreviousStories />} />

      </Routes>
    </Router>
  );
}

export default App;




