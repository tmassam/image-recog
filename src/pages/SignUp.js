import React, { useState } from 'react';
import { signUp, resendConfirmationCode } from '../Components/Cognito';
import '../Styles/Auth.css';
import { Link, useNavigate } from 'react-router-dom';

const SignUp = () => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [statusMessage, setStatusMessage] = useState('');

    const onResendConfirmationCode = async (e) => {
        e.preventDefault();
        setStatusMessage('');
    
        try {
          await resendConfirmationCode(username);
          setStatusMessage('Confirmation code resent. Please check your email.');
        } catch (error) {
          console.error('Error resending confirmation code:', error);
          setStatusMessage('Error resending confirmation code: ' + error.message);
        }
      };
  
      const navigate = useNavigate();

      const onSubmit = async (e) => {
        e.preventDefault();
        setStatusMessage('');
    
        try {
          await signUp(username, password, email);
          setStatusMessage('User signed up successfully. Please check your email for the confirmation code.');
          navigate('/confirm'); // Navigate to the confirmation page
        } catch (error) {
          console.error('Error signing up user:', error);
          setStatusMessage('Error signing up user: ' + error.message);
        }
      };

  return (
    <div>
         <header>
      <h1>Image Recognition App</h1>
    </header>
    <div className="auth-container">
    <h2>Sign Up</h2>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <input
          type="email"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <button type="submit">Sign Up</button>
      </form>
      <p>
        Already have an account? <Link to="/signin">Sign In</Link>
      </p>
      <button onClick={onResendConfirmationCode}>Resend Confirmation Code</button>
      {statusMessage && <p className="status-message">{statusMessage}</p>}
    </div>
    </div>
  );
};

export default SignUp;
