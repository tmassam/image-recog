import React, { useState } from 'react';
import { signIn } from '../Components/Cognito';
import '../Styles/Auth.css';
import { Link, useNavigate } from 'react-router-dom';

const SignIn = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [statusMessage, setStatusMessage] = useState('');
  const navigate = useNavigate();

  const onSubmit = async (e) => {
    e.preventDefault();
    setStatusMessage('');
  
    try {
      await signIn(username, password);
      setStatusMessage('User signed in successfully');
      navigate('/dashboard'); // navigate to the desired page after successful sign-in
    } catch (error) {
      console.error('Error signing in user:', error);
      if (error.code === 'UserNotConfirmedException') {
        setStatusMessage('User is not confirmed. Please check your email for the confirmation code.');
      } else {
        setStatusMessage('Error signing in user: ' + error.message);
      }
    }
  };

    return (
        <div>
        <header>
     <h1>Image Recognition App</h1>
   </header>
   <div className="auth-container">
          <h2>Sign In</h2>
          <form onSubmit={onSubmit}>
            <input
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <button type="submit">Sign In</button>
          </form>
          <p>
            Don't have an account? <Link to="/signup">Sign Up</Link>
          </p>
          {statusMessage && <p className="status-message">{statusMessage}</p>}
        </div>
        </div>
      );
    };
    
    export default SignIn;

