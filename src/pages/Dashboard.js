import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { styled } from '@mui/styles';
import { Card, CardActionArea, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import { Face, MenuBook } from '@mui/icons-material';
import Auth from '@aws-amplify/auth';
import Navbar from '../Components/Navbar';


const useStyles = () => {
  const cardContainer = styled(Grid)({
    marginTop: 8,
    marginBottom: 8,
  });

  const card = styled(Card)({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 200,
  });

  const icon = styled(Face)({
    fontSize: 60,
  });

  return { cardContainer, card, icon };
};

const Dashboard = () => {
  const classes = useStyles();
  const [username, setUsername] = useState('');

  useEffect(() => {
    async function fetchUser() {
      const user = await Auth.currentAuthenticatedUser();
      setUsername(user.username);
    }

    fetchUser();
  }, []);

  return (
    <>
    <Navbar />
    <div className={classes.root}>
      <Typography variant="h4" component="h2" gutterBottom>
        Hello, {username}
      </Typography>
      <Typography variant="body1" gutterBottom>
        Welcome to the Image Recognition App! Our application uses Amazon Rekognition and OpenAI API to provide you with skin
        recognition and story recognition services. Simply choose a service below and start exploring!
      </Typography>

      <Grid container spacing={4}>
        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            <CardActionArea component={Link} to="/skin">
              <CardMedia
                className={classes.media}
                image="https://source.unsplash.com/random?skin"
                title="Skin Recognition"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  <Face fontSize="large" /> Skin Recognition
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Analyze skin conditions with our advanced skin recognition service. Powered by Amazon Rekognition.
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <Card className={classes.card}>
            <CardActionArea component={Link} to="/story">
              <CardMedia
                className={classes.media}
                image="https://source.unsplash.com/random?story"
                title="Story Recognition"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  <MenuBook fontSize="large" /> Story Recognition
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Generate stories based on images using our story recognition service. Powered by OpenAI API.
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
    </div>
    </>
  );
};

export default Dashboard;
