import React, { useState, useEffect } from 'react';
import { Container, Typography, Box, Paper } from '@mui/material';
import { authenticateUser } from '../Components/Cognito';
import { getUserStories } from '../Components/DynamoDbHelper';
import Navbar from '../Components/Navbar';

function PreviousStories() {
  const [stories, setStories] = useState([]);

  useEffect(() => {
    const fetchUserStories = async () => {
        try {
            const userId = await authenticateUser();
            const response = await fetch('https://1yckdbh662.execute-api.us-east-1.amazonaws.com/prod/get-dynamo', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${userId.token}`
                }
            });
            const userStories = await response.json();
            setStories(userStories);
        } catch (error) {
            console.error('Error fetching user stories:', error);
        }
    };

    fetchUserStories();
}, []);


  return (
    <>
    <Navbar />
    <Container>
      <Typography variant="h4" gutterBottom>
        Your Previous Stories
      </Typography>
      {stories.map((storyItem, index) => (
        <Box key={index} mb={4}>
          <Paper elevation={3} style={{ padding: 16 }}>
            <Typography variant="h5" gutterBottom>
              Story {index + 1}
            </Typography>
            <Typography>{storyItem.story}</Typography>
            <Box mt={2}>
              <img src={storyItem.image} alt={`story ${index + 1}`} style={{ maxWidth: '100%' }} />
            </Box>
          </Paper>
        </Box>
      ))}
    </Container>
    </>
  );
}

export default PreviousStories;
