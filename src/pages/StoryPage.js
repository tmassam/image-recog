import React, { useState, useEffect } from 'react';
import { uploadImageAndGenerateStory } from '../APIGateway';
import { withAuthenticator } from '@aws-amplify/ui-react';
import { authenticateUser } from '../Components/Cognito';
import { Container, Typography, Box, Paper } from '@mui/material';
import ImageUploader from '../Components/ImageUploader';
import Navbar from '../Components/Navbar';

function StoryPage() {
  const [imageUrl, setImageUrl] = useState('');
  const [story, setStory] = useState('');

  useEffect(() => {
    const getUserCredentials = async () => {
      try {
        const credentials = await authenticateUser();
        console.log('User credentials:', credentials);
      } catch (error) {
        console.error('Error getting user credentials:', error);
      }
    };

    getUserCredentials();
  }, []);

  // function to handle the image upload event from the ImageUploader component
  async function onImageUpload(imageData) {
    const { story } = await uploadImageAndGenerateStory(imageData.s3BucketName, imageData.s3Key);
    setImageUrl(imageData.imageUrl);
    setStory(story);
  }

  return (
    <>
    <Navbar />
    <Container>
      <Typography variant="h4" gutterBottom>
        Image Recognition Demo
      </Typography>
      <Typography paragraph>
        This demo uses the Amazon Rekognition API to detect labels in an uploaded image. It then uses the OpenAI API to generate a story based on the labels.
      </Typography>
      <Box mb={4}>
        <ImageUploader onUpload={onImageUpload} />
      </Box>
      {imageUrl && (
        <Box mb={4}>
          <img src={imageUrl} alt="uploaded" style={{ maxWidth: '100%' }} />
        </Box>
      )}
      {story && (
        <Paper elevation={3} style={{ padding: 16 }}>
          <Typography variant="h5" gutterBottom>
            Story
          </Typography>
          <Typography>{story}</Typography>
        </Paper>
      )}
      <Box mt={4}>
        <Typography variant="h5" gutterBottom>
          How it works
        </Typography>
        <Typography>
          This website uses advanced AI technology to generate a story based on the labels detected in an uploaded image. Simply upload an image and let our algorithms do the rest.
        </Typography>
      </Box>
    </Container>
    </>
  );
}

export default withAuthenticator(StoryPage);

