import React, { useState } from 'react';
import AWS from 'aws-sdk';
import axios from 'axios';
import { FaUpload } from "react-icons/fa";
import '..';
import Navbar from '../Components/Navbar';

AWS.config.update({ region: 'us-east-1' });

AWS.config.update({
  region: 'us-east-1',
  accessKeyId: 'AKIARVXDVJXZCF7CKW6E',
  secretAccessKey: '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',
});

const rekognition = new AWS.Rekognition({ apiVersion: '2016-06-27' });
const openaiApiKey = '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';

function SkinPage() {
  const [imageUrl, setImageUrl] = useState('');
  const [labels, setLabels] = useState([]);
  const [treatmentOptions, setTreatmentOptions] = useState('');

  async function handleUpload(event) {
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = async () => {
      const imageBytes = new Uint8Array(reader.result);
      const result = await rekognition.detectCustomLabels({
        Image: {
          Bytes: imageBytes,
        },
        ProjectVersionArn: 'arn:aws:rekognition:us-east-1:115367824882:project/skinAnalysis/version/skinAnalysis.2023-03-19T22.42.02/1679280122131',
        MaxResults: 10,
      }).promise();
      setLabels(result.CustomLabels);
      const labelNames = result.CustomLabels.map(label => label.Name.toLowerCase());
      const treatmentOptions = await generateTreatmentAdvice(labelNames.join(', '));
      setTreatmentOptions(treatmentOptions);
    };
    reader.readAsArrayBuffer(file);
    setImageUrl(URL.createObjectURL(file));
  }

  async function generateTreatmentAdvice(labelNames) {
    console.log("Calling the OpenAI API for treatment advice");
    
    const prompt = "What are the best treatment options for " + labelNames + "?";
    
    const APIBody = {
      "model": "text-davinci-002",
      "prompt": prompt,
      "temperature": 0.5,
      "max_tokens": 250,
      "top_p": 1.0,
      "frequency_penalty": 0.0,
      "presence_penalty": 0.0
    }

    const response = await axios.post("https://api.openai.com/v1/completions", APIBody, {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${openaiApiKey}`
      }
    });
    
    const treatmentAdvice = response.data.choices[0].text.trim();
    return treatmentAdvice;
  }

  return (
    <>
    <Navbar />
    <div className="container">
      <header>
        <h1 className="title">Amazon Rekognition and OpenAI Demo</h1>
      </header>
      <div className="description">
        <p>This demo uses the Amazon Rekognition API to detect labels in an uploaded image. It then uses the OpenAI GPT-3 API to provide personalized treatment advice based on those labels.</p>
      </div>
      <div className="upload-section">
        <div className="upload-btn-wrapper">
          <button className="btn">
            <FaUpload className="upload-icon" />
            Upload an Image
          </button>
          <input type="file" onChange={handleUpload} />
        </div>
        {imageUrl && <img src={imageUrl} alt="uploaded" />}
      </div>
      {labels.map(label => (
        <div key={label.Name} className="label-card">
          <h2>{label.Name}</h2>
          <p>Confidence: {label.Confidence.toFixed(2)}</p>
          <h2>Treatment Advice:</h2>
          <p>{treatmentOptions}</p>
        </div>
      ))}
      <div className="box">
        <h2>How it works</h2>
        <p>This website uses advanced AI technology to provide personalized treatment advice for your medical needs. Simply upload an image and let our algorithms do the rest.</p>
      </div>
    </div>
    </>
  );
}

export default SkinPage;