import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { confirmSignUp } from '../Components/Cognito';
import '../Styles/Auth.css';
import { FiMail } from 'react-icons/fi'; 

const ConfirmSignUp = () => {
  const [username, setUsername] = useState('');
  const [confirmationCode, setConfirmationCode] = useState('');
  const [statusMessage, setStatusMessage] = useState('');

  const onSubmit = async (e) => {
    e.preventDefault();
    setStatusMessage('');

    try {
      await confirmSignUp(username, confirmationCode);
      setStatusMessage('Account confirmed! You can now sign in.');
    } catch (error) {
      console.error('Error confirming account:', error);
      setStatusMessage('Error confirming account: ' + error.message);
    }
  };

  return (
    <div className="confirmation-container auth-container">
      <div className="auth-icon">
        <FiMail size={48} /> 
      </div>
      <h1>Confirm Sign Up</h1>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="text"
          placeholder="Confirmation Code"
          value={confirmationCode}
          onChange={(e) => setConfirmationCode(e.target.value)}
        />
        <button type="submit">Confirm</button>
      </form>
      <p className="status-message">{statusMessage}</p>
      <p>
        Already confirmed? <Link to="/signin">Sign In</Link>
      </p>
    </div>
  );
};

export default ConfirmSignUp;

