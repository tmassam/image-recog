import axios from 'axios';

async function uploadImageAndGenerateStory(bucket, key) {
  const apiUrl = 'https://1yckdbh662.execute-api.us-east-1.amazonaws.com/prod/image-story';

  try {
    const response = await axios.post(apiUrl, JSON.stringify({ bucket, key }), {
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const story = response.data.story;
    console.log('Generated story:', story);
    return { imageUrl: `https://${bucket}.s3.amazonaws.com/${key}`, story };
  } catch (error) {
    console.error('Error generating story:', error);
    throw error;
  }
}

export { uploadImageAndGenerateStory };

  
