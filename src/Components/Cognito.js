import { Auth, Amplify } from 'aws-amplify';

const awsconfig = {
  Auth: {
    identityPoolId: 'us-east-1:07101d3d-a431-4dc3-9e2a-af139b66bf93',
    region: 'us-east-1',
    userPoolId: 'us-east-1_pqa86kXhs',
    userPoolWebClientId: '16igp4v08req0mh72fhb90d6j5',
  },
};

Amplify.configure(awsconfig);

export const authenticateUser = async () => {
  try {
    const credentials = await Auth.currentCredentials();
    return credentials;
  } catch (error) {
    console.error('Error retrieving user credentials', error);
  }
};

export { awsconfig };


export const signUp = async (username, password, email) => {
    try {
      const trimmedEmail = email.trim();
      console.log('Signing up with email:', trimmedEmail);
      const result = await Auth.signUp({
        username,
        password,
        attributes: {
          email: trimmedEmail,
        },
      });
      console.log('Sign up success!', result);
      return result;
    } catch (error) {
      console.error('Error signing up:', error);
      throw error;
    }
  };
  
  
  export const signIn = async (username, password) => {
    try {
      const user = await Auth.signIn(username, password);
      console.log('Sign in success!', user);
      return user;
    } catch (error) {
      console.error('Error signing in:', error);
      throw error;
    }
  };
  
  export const resendConfirmationCode = async (username) => {
    try {
      await Auth.resendSignUp(username);
      console.log('Confirmation code resent successfully');
    } catch (error) {
      console.error('Error resending confirmation code:', error);
      throw error;
    }
  };

  export const confirmSignUp = async (username, confirmationCode) => {
    try {
      await Auth.confirmSignUp(username, confirmationCode);
      console.log('User confirmed successfully');
    } catch (error) {
      console.error('Error confirming user:', error);
      throw error;
    }
  };
