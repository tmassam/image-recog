import React, { useContext } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './Navbar.css';
import { Auth } from 'aws-amplify';
import AuthContext from './AuthContext';

const Navbar = () => {
    const navigate = useNavigate();
    const { setIsAuthenticated } = useContext(AuthContext);
  
    const handleLogout = async () => {
      try {
        await Auth.signOut();
        setIsAuthenticated(false);
        navigate('/signin');
      } catch (error) {
        console.error('Error signing out:', error);
      }
    };
  return (
    <nav className="navbar">
      <ul className="navbar-nav">
        <li className="nav-item">
          <NavLink to="/dashboard" className="nav-link">
            Dashboard
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/story" className="nav-link">
            Story Generator
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/skin" className="nav-link">
            Skin Analysis
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/previous-stories" className="nav-link">
            Previous Stories
          </NavLink>
        </li>
        <li className="nav-item">
        <button onClick={handleLogout} className="nav-link logout-button">
            Logout
          </button>
        </li>
        {/* Add more links as needed */}
      </ul>
    </nav>
  );
};

export default Navbar;
