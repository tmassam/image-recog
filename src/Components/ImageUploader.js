import React from 'react';
import { Storage } from 'aws-amplify';
import { Button } from '@mui/material';
import { styled } from '@mui/system';
import { Auth } from 'aws-amplify';

async function getJwtToken() {
  const session = await Auth.currentSession();
  return session.getIdToken().getJwtToken();
}

const Input = styled('input')({
  display: 'none',
});

async function uploadImageToS3(file) {
  const result = await Storage.put(file.name, file, {
    contentType: file.type,
  });
  return result;
}

async function uploadImageAndGenerateStory(imageUrl, s3Key, s3BucketName) {
    const jwtToken = await getJwtToken();
  
    const response = await fetch('https://1yckdbh662.execute-api.us-east-1.amazonaws.com/prod/image-story', {
      method: 'POST',
      body: JSON.stringify({
        key: s3Key,
        bucket: s3BucketName,
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${jwtToken}`,
      }
    });
  
    const json = await response.json();
    return json;
  }
  
  
  function ImageUploader({ onUpload }) {
    async function handleUpload(event) {
      event.preventDefault();
      const file = event.target.files[0];
      const result = await uploadImageToS3(file);
      
  
      // Get the image URL and S3 key from Amplify Storage
      const imageUrl = await Storage.get(result.key);
      const s3Key = result.key;
  
      // Pass the s3BucketName to the uploadImageAndGenerateStory function
      const s3BucketName = process.env.REACT_APP_S3_BUCKET_NAME;
      const response = await uploadImageAndGenerateStory(s3BucketName, s3Key);
      const { story } = response;
  
      onUpload({ s3BucketName, s3Key, story, imageUrl });
    }

  return (
    <label htmlFor="contained-button-file">
      <Input
        accept="image/*"
        id="contained-button-file"
        type="file"
        onChange={handleUpload}
      />
      <Button variant="contained" component="span">
        Upload an Image
      </Button>
    </label>
  );
}

export default ImageUploader;
