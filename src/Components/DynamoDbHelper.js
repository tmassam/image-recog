import AWS from 'aws-sdk';
import { awsconfig } from './Cognito';

AWS.config.update({
  region: awsconfig.Auth.region,
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: awsconfig.Auth.identityPoolId,
  })
});

const dynamoDbInstance = new AWS.DynamoDB.DocumentClient();

export const saveUserStory = async (userId, story, image) => {
  const timestamp = Date.now();

  const params = {
    TableName: 'UserStories',
    Item: {
      userId,
      timestamp,
      story,
      image,
    },
  };

  try {
    await dynamoDbInstance.put(params).promise();
    return true;
  } catch (error) {
    console.error('Error saving user story:', error);
    return false;
  }
};

export const getUserStories = async (userId) => {
  const params = {
    TableName: 'UserStories',
    KeyConditionExpression: 'userId = :userId',
    ExpressionAttributeValues: {
      ':userId': userId,
    },
  };

  try {
    const result = await dynamoDbInstance.query(params).promise();
    return result.Items;
  } catch (error) {
    console.error('Error fetching user stories:', error);
    return [];
  }
};


const cognito = new AWS.CognitoIdentityServiceProvider({
  region: awsconfig.Auth.region,
});

export const getCurrentUserId = async () => {
  const accessToken = await new Promise((resolve, reject) => {
    const user = cognito.getCurrentUser();
    if (user === null) {
      reject(new Error('No authenticated user found'));
    }
    user.getSession((err, session) => {
      if (err) {
        reject(err);
      } else {
        resolve(session.getAccessToken().getJwtToken());
      }
    });
  });

  const params = {
    AccessToken: accessToken,
  };

  try {
    const result = await cognito.getUser(params).promise();
    return result.UserAttributes.find(attr => attr.Name === 'sub').Value;
  } catch (error) {
    console.error('Error fetching user ID:', error);
    return null;
  }
};




