import { Route, Outlet, Navigate } from 'react-router-dom';
import { useAuth } from './AuthContext';

function PrivateRoute({ path }) {
  const { currentUser } = useAuth();

  return currentUser ? (
    <Route path={path} element={<Outlet />} />
  ) : (
    <Navigate to="/signin" />
  );
}

export default PrivateRoute;

