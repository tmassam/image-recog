import openai from 'openai';
const { Configuration, OpenAIApi } = require("openai");

const configuration = new Configuration({
  apiKey: "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",
});

export async function generateTreatmentAdvice(labels) {
  const openai = new OpenAIApi(configuration)
  const completion = await openai.createCompletion({
    engine: 'davinci',
    prompt: `Given the following labels: ${labels}, what are some treatment options?`,
    maxTokens: 100,
    n: 1,
    stop: ['\n'],
    temperature: 0.7
  });
  console.log(completion.data.choices[0].text);
}


