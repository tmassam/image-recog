import React from 'react';
import ReactDOM from 'react-dom/client';
import './Components/Cognito';
import App from './App';
import { Amplify } from 'aws-amplify';
import { awsconfig } from './Components/Cognito';
import { AuthProvider } from './Components/AuthContext';

// Extend the awsconfig with Storage configuration
const extendedAwsConfig = {
  ...awsconfig,
  Storage: {
    AWSS3: {
      bucket: 'image-recog-labels',
      region: 'us-east-1',
    },
  },
};

// Configure Amplify
Amplify.configure(extendedAwsConfig);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
     <AuthProvider>
      <App />
    </AuthProvider>
  </React.StrictMode>
);


